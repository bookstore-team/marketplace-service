package ru.book.store.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StartPageController {
    @GetMapping("/")
    public String gatStartPage() {
        return "Стартовая страница магазина";
    }
}
